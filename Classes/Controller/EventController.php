<?php
namespace HIVE\HiveExtEvent\Controller;

/***
 *
 * This file is part of the "hive_ext_event" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 ***/

/**
 * EventController
 */
class EventController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * eventRepository
     *
     * @var \HIVE\HiveExtEvent\Domain\Repository\EventRepository
     * @inject
     */
    protected $eventRepository = null;

    /**
     * initializeAction
     *
     * @return void
     */
    public function initializeAction()
    {
        $oSettings = $this->settings;
        $this->eventRepository->updateEventStatus();
        if ($oSettings['list']['bHidePastEventsAutomatically']) {
            $this->eventRepository->hidePastEvents();
        }
    }

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $aSettings = $this->settings;
        $iLimit = $aSettings['iLimit'];
        $sOrder = $aSettings['sOrder'];
        $iFilteredCategories = $aSettings['categories'];
        //        var_dump($oSettings);die;
        $oEvents = $this->eventRepository->findUpcomingEvents($GLOBALS['TSFE']->sys_language_uid, $iLimit, $sOrder, $iFilteredCategories);
        $this->view->assignMultiple([
            'oEvents' => $oEvents,
            'oSettings' => $aSettings
        ]);
    }

    /**
     * action show
     *
     * @param \HIVE\HiveExtEvent\Domain\Model\Event $oEvent
     * @return void
     */
    public function showAction(\HIVE\HiveExtEvent\Domain\Model\Event $oEvent)
    {
        $this->view->assign('oEvent', $oEvent);
    }

    /**
     * action renderSlider
     *
     * @return void
     */
    public function renderSliderAction()
    {
        // get settings
        $aSettings = $this->settings;
        $iLimit = $aSettings['iLimit'];
        $sOrder = $aSettings['sOrder'];
        $iFilteredCategories = $aSettings['categories'];
        // get plugin uid
        $iPluginUid = $this->configurationManager->getContentObject()->data['uid'];
        // get selected histories if not empty
        $oEvents = $this->eventRepository->findUpcomingEvents($GLOBALS['TSFE']->sys_language_uid, $iLimit, $sOrder, $iFilteredCategories);
        //render slides to array
        $aSlides = [];
        foreach ($oEvents as $oSlide) {
            $configuration = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
            $extensionName = $this->request->getControllerExtensionName();
            $standaloneView = $this->objectManager->get('TYPO3\\CMS\\Fluid\\View\\StandaloneView');
            $standaloneView->getRequest()->setControllerExtensionName($extensionName);
            $standaloneView->setFormat('html');
            //$standaloneView->setLayoutRootPaths($configuration['view']['layoutRootPaths']);
            $standaloneView->setPartialRootPaths($configuration['view']['partialRootPaths']);
            $standaloneView->setTemplateRootPaths($configuration['view']['templateRootPaths']);
            $standaloneView->setTemplate('Event/RenderEvent');
            //$aSlide = json_decode(json_encode($oSlide), true);
            $standaloneView->assign('settings', $aSettings);
            $standaloneView->assign('event', $oSlide);
            $result = $standaloneView->render();
            $aSlides[] = $result;
        }
        $this->view->assign('aSlides', $aSlides);
        $this->view->assign('aSettings', $aSettings);
        $this->view->assign('iPluginUid', $iPluginUid);
    }
}
