<?php
namespace HIVE\HiveExtEvent\Domain\Repository;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
/***
 *
 * This file is part of the "hive_ext_event" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 ***/

/**
 * The repository for Events
 */
class EventRepository extends \HIVE\HiveCfgRepository\Domain\Repository\AbstractRepository
{
    const DATABASE_EVENT = 'tx_hiveextevent_domain_model_event';

    const DATABASE_SYSCATEGORY = 'sys_category';

    const DATABASE_SYSCATEGORY_EVENT = 'sys_category_record_mm';

    /**
     * eventRepository
     *
     * @var \HIVE\HiveExtEvent\Domain\Repository\EventRepository
     * @inject
     */
    protected $eventRepository = null;

    /**
     * initializeObject
     */
    public function initializeObject()
    {
        $sUserFuncModel = 'HIVE\\HiveExtEvent\\Domain\\Model\\Event';
        $sUserFuncPlugin = 'tx_hiveextevent';
        parent::overrideQuerySettings($sUserFuncModel, $sUserFuncPlugin);
    }

    /**
     * hidePastEvents
     */
    public function hidePastEvents()
    {
        $dCurrentDate = time();
        $oCleanUpQueryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(self::DATABASE_EVENT);
        $oCleanUpQueryBuilder->update(self::DATABASE_EVENT)->where($oCleanUpQueryBuilder->expr()->lt('end_date', $oCleanUpQueryBuilder->createNamedParameter($dCurrentDate)), $oCleanUpQueryBuilder->expr()->orX($oCleanUpQueryBuilder->expr()->eq('intervalvalue', $oCleanUpQueryBuilder->createNamedParameter(0, \PDO::PARAM_INT)), $oCleanUpQueryBuilder->expr()->eq('intervalunit', $oCleanUpQueryBuilder->createNamedParameter(0, \PDO::PARAM_INT))))->set('hidden', 1)->execute();
    }

    /**
     * updateEventStatus
     */
    public function updateEventStatus()
    {
        $dCurrentDateTime = time();
        $dCurrentDate = new \DateTime();
        $dCurrentDate->setTimestamp($dCurrentDateTime);
        $oRepository = $this->eventRepository;
        $oUpdateQueryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(self::DATABASE_EVENT);
        $oUpdateQueryBuilder->select(self::DATABASE_EVENT . '.uid', self::DATABASE_EVENT . '.start_date', self::DATABASE_EVENT . '.end_date', self::DATABASE_EVENT . '.intervalvalue', self::DATABASE_EVENT . '.intervalunit')->from(self::DATABASE_EVENT)->where($oUpdateQueryBuilder->expr()->lt('end_date', $oUpdateQueryBuilder->createNamedParameter($dCurrentDateTime)), $oUpdateQueryBuilder->expr()->neq('intervalvalue', $oUpdateQueryBuilder->createNamedParameter(0, \PDO::PARAM_INT)), $oUpdateQueryBuilder->expr()->neq('intervalunit', $oUpdateQueryBuilder->createNamedParameter(0, \PDO::PARAM_INT)));
        // get all potential interval entries
        $oResults = $oUpdateQueryBuilder->execute()->fetchAll();
        // loop all results and change date like set in interval
        foreach ($oResults as $o) {
            $oEvent = $this->findByUid($o['uid']);
            $iIntervalValue = $o['intervalvalue'];
            switch ($o['intervalunit']) {
                case 2:    $sIntervalUnit = 'W';
                    break;
                case 3:    $sIntervalUnit = 'M';
                    break;
                case 4:    $sIntervalUnit = 'Y';
                    break;
                default:    $sIntervalUnit = 'D';
                    //                    $interval = $oEvent->getEndDate()->diff($dCurrentDate);
                    //                    var_dump($dCurrentDate);
                    //                    var_dump($oEvent->getEndDate());
                    //                    var_dump($interval);
                    break;
            }
            $this->updateDateWithInterval($oEvent, $o['start_date'], $iIntervalValue, $sIntervalUnit, 'start_date');
            $this->updateDateWithInterval($oEvent, $o['end_date'], $iIntervalValue, $sIntervalUnit, 'end_date');
        }
    }

    /**
     * updateDateWithInterval
     *
     * @param \HIVE\HiveExtEvent\Domain\Model\Event $oEvent
     * @param $dOriginDateTimestamp
     * @param $iIntervalValue
     * @param $sIntervalUnit
     * @param $sDateIdentifier
     */
    public function updateDateWithInterval(\HIVE\HiveExtEvent\Domain\Model\Event $oEvent, $dOriginDateTimestamp, $iIntervalValue, $sIntervalUnit, $sDateIdentifier)
    {
        if (($sDateIdentifier == 'start_date' || $sDateIdentifier == 'end_date') && ($sIntervalUnit == 'D' || $sIntervalUnit == 'W' || $sIntervalUnit == 'M' || $sIntervalUnit == 'Y') && is_numeric($iIntervalValue) && is_numeric($dOriginDateTimestamp)) {
            $oRepository = $this->eventRepository;
            $dOriginDate = new \DateTime();
            $dOriginDate->setTimestamp($dOriginDateTimestamp);
            if ($sIntervalUnit == 'W') {
                $dInterval = new \DateInterval('P' . $iIntervalValue * 7 . 'D');
            } else {
                $dInterval = new \DateInterval('P' . $iIntervalValue . $sIntervalUnit);
            }
            $dIntervalDate = $dOriginDate;
            $dIntervalDate->add($dInterval);
            if ($sDateIdentifier == 'start_date') {
                $oEvent->setStartDate($dIntervalDate);
            } else {
                $oEvent->setEndDate($dIntervalDate);
            }
            $oRepository->update($oEvent);
        }
    }

    /**
     * findUpcomingEvents
     *
     * @param int $iSysLanguageUid
     * @param int $iLimit
     * @param string $sOrderDirection
     * @param int $sFilteredCategories
     * @return \HIVE\HiveExtEvent\Domain\Model\Event
     */
    public function findUpcomingEvents($iSysLanguageUid, $iLimit, $sOrderDirection, $sFilteredCategories)
    {
        if ($sFilteredCategories) {
            // get data from database
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(self::DATABASE_EVENT);
            $queryBuilder->select(self::DATABASE_EVENT . '.uid', self::DATABASE_SYSCATEGORY . '.uid', self::DATABASE_SYSCATEGORY . '.title', self::DATABASE_SYSCATEGORY_EVENT . '.uid_local', self::DATABASE_SYSCATEGORY_EVENT . '.uid_foreign')->from(self::DATABASE_EVENT)->leftJoin(self::DATABASE_EVENT, self::DATABASE_SYSCATEGORY_EVENT, self::DATABASE_SYSCATEGORY_EVENT, $queryBuilder->expr()->eq(self::DATABASE_EVENT . '.uid', $queryBuilder->quoteIdentifier(self::DATABASE_SYSCATEGORY_EVENT . '.uid_foreign')))->leftJoin(self::DATABASE_SYSCATEGORY_EVENT, self::DATABASE_SYSCATEGORY, self::DATABASE_SYSCATEGORY, $queryBuilder->expr()->eq(self::DATABASE_SYSCATEGORY . '.uid', $queryBuilder->quoteIdentifier(self::DATABASE_SYSCATEGORY_EVENT . '.uid_local')), $queryBuilder->expr()->eq(self::DATABASE_EVENT . '.sys_language_uid', $queryBuilder->quoteIdentifier($iSysLanguageUid)))->where($queryBuilder->expr()->eq(self::DATABASE_SYSCATEGORY_EVENT . '.tablenames', $queryBuilder->createNamedParameter('tx_hiveextevent_domain_model_event')));
            // look for selected sys_categories
            $aFilterCategories = explode(',', $sFilteredCategories);
            if (count($aFilterCategories) > 1) {
                foreach ($aFilterCategories as $iKey => $iValue) {
                    if ($iKey == 0) {
                        $queryBuilder->andWhere($queryBuilder->expr()->eq(self::DATABASE_SYSCATEGORY_EVENT . '.uid_local', $queryBuilder->createNamedParameter($iValue, \PDO::PARAM_INT)));
                    } else {
                        $queryBuilder->orWhere($queryBuilder->expr()->eq(self::DATABASE_SYSCATEGORY_EVENT . '.uid_local', $queryBuilder->createNamedParameter($iValue, \PDO::PARAM_INT)));
                    }
                }
            } else {
                $queryBuilder->andWhere($queryBuilder->expr()->eq(self::DATABASE_SYSCATEGORY_EVENT . '.uid_local', $queryBuilder->createNamedParameter($aFilterCategories[0], \PDO::PARAM_INT)));
            }
            // set limit
            if ($iLimit) {
                $queryBuilder->setMaxResults($iLimit);
            }
            // ordering
            $queryBuilder->orderBy('tx_hiveextevent_domain_model_event.start_date', $sOrderDirection == 'desc' ? 'DESC' : 'ASC');
            // execute query
            $oResults = $queryBuilder->execute()->fetchAll();
            // loop result rows
            $oObject = [];
            foreach ($oResults as $o) {
                $oObject[] = $this->findByUid($o['uid_foreign']);
            }
            return $oObject;
        } else {
            // init query
            $query = $this->createQuery();
            // limit query to given number of events
            if ($iLimit) {
                $query->setLimit(intval($iLimit));
            }
            // set ordering of events
            switch ($sOrderDirection) {
                case 'desc':    $query->setOrderings([
                        'start_date' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING
                    ]);
                    break;
                default:    $query->setOrderings([
                        'start_date' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
                    ]);
                    break;
            }
            // execute query
            return $query->execute();
        }
    }
}
