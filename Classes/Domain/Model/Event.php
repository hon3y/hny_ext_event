<?php
namespace HIVE\HiveExtEvent\Domain\Model;

/***
 *
 * This file is part of the "hive_ext_event" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 ***/

/**
 * Event
 */
class Event extends \HIVE\HiveExtArticle\Domain\Model\Article
{
    /**
     * logo
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $logo = null;

    /**
     * zip
     *
     * @var string
     */
    protected $zip = '';

    /**
     * city
     *
     * @var string
     */
    protected $city = '';

    /**
     * street
     *
     * @var string
     */
    protected $street = '';

    /**
     * location
     *
     * @var string
     */
    protected $location = '';

    /**
     * country
     *
     * @var string
     */
    protected $country = '';

    /**
     * building
     *
     * @var string
     */
    protected $building = '';

    /**
     * room
     *
     * @var string
     */
    protected $room = '';

    /**
     * booth
     *
     * @var string
     */
    protected $booth = '';

    /**
     * startDate
     *
     * @var \DateTime
     */
    protected $startDate = null;

    /**
     * endDate
     *
     * @var \DateTime
     */
    protected $endDate = null;

    /**
     * url
     *
     * @var string
     */
    protected $url = '';

    /**
     * document
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $document = null;

    /**
     * intervalvalue
     *
     * @var int
     */
    protected $intervalvalue = 0;

    /**
     * intervalunit
     *
     * @var int
     */
    protected $intervalunit = 0;

    /**
     * Returns the logo
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $logo
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Sets the logo
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $logo
     * @return void
     */
    public function setLogo(\TYPO3\CMS\Extbase\Domain\Model\FileReference $logo)
    {
        $this->logo = $logo;
    }

    /**
     * Returns the zip
     *
     * @return string $zip
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Sets the zip
     *
     * @param string $zip
     * @return void
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * Returns the city
     *
     * @return string $city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Sets the city
     *
     * @param string $city
     * @return void
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * Returns the street
     *
     * @return string $street
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Sets the street
     *
     * @param string $street
     * @return void
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * Returns the location
     *
     * @return string $location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Sets the location
     *
     * @param string $location
     * @return void
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * Returns the country
     *
     * @return string $country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Sets the country
     *
     * @param string $country
     * @return void
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * Returns the building
     *
     * @return string $building
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * Sets the building
     *
     * @param string $building
     * @return void
     */
    public function setBuilding($building)
    {
        $this->building = $building;
    }

    /**
     * Returns the room
     *
     * @return string $room
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * Sets the room
     *
     * @param string $room
     * @return void
     */
    public function setRoom($room)
    {
        $this->room = $room;
    }

    /**
     * Returns the booth
     *
     * @return string $booth
     */
    public function getBooth()
    {
        return $this->booth;
    }

    /**
     * Sets the booth
     *
     * @param string $booth
     * @return void
     */
    public function setBooth($booth)
    {
        $this->booth = $booth;
    }

    /**
     * Returns the startDate
     *
     * @return \DateTime $startDate
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Sets the startDate
     *
     * @param \DateTime $startDate
     * @return void
     */
    public function setStartDate(\DateTime $startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * Returns the endDate
     *
     * @return \DateTime $endDate
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Sets the endDate
     *
     * @param \DateTime $endDate
     * @return void
     */
    public function setEndDate(\DateTime $endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * Returns the url
     *
     * @return string $url
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Sets the url
     *
     * @param string $url
     * @return void
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * Returns the document
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $document
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Sets the document
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $document
     * @return void
     */
    public function setDocument(\TYPO3\CMS\Extbase\Domain\Model\FileReference $document)
    {
        $this->document = $document;
    }

    /**
     * Returns the intervalvalue
     *
     * @return int $intervalvalue
     */
    public function getIntervalvalue()
    {
        return $this->intervalvalue;
    }

    /**
     * Sets the intervalvalue
     *
     * @param string $intervalvalue
     * @return void
     */
    public function setIntervalvalue($intervalvalue)
    {
        $this->intervalvalue = $intervalvalue;
    }

    /**
     * Returns the intervalunit
     *
     * @return int $intervalunit
     */
    public function getIntervalunit()
    {
        return $this->intervalunit;
    }

    /**
     * Sets the intervalunit
     *
     * @param string $intervalunit
     * @return void
     */
    public function setIntervalunit($intervalunit)
    {
        $this->intervalunit = $intervalunit;
    }
}
