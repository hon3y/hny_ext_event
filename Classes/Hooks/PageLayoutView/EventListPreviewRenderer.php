<?php
namespace HIVE\HiveExtEvent\Hooks\PageLayoutView;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use \TYPO3\CMS\Backend\View\PageLayoutViewDrawItemHookInterface;
use \TYPO3\CMS\Backend\View\PageLayoutView;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;


/**
 * Contains a preview rendering for the page module of CType="yourextensionkey_newcontentelement"
 */
class EventListPreviewRenderer implements PageLayoutViewDrawItemHookInterface
{

    /**
     * Preprocesses the preview rendering of a content element of type "My new content element"
     *
     * @param \TYPO3\CMS\Backend\View\PageLayoutView $parentObject Calling parent object
     * @param bool $drawItem Whether to draw the item using the default functionality
     * @param string $headerContent Header content
     * @param string $itemContent Item content
     * @param array $row Record row of tt_content
     *
     * @return void
     */
    public function preProcess(
        PageLayoutView &$parentObject,
        &$drawItem,
        &$headerContent,
        &$itemContent,
        array &$row
    )
    {

        if ($row['list_type'] !== 'hiveextevent_hiveexteventeventlist' && $row['list_type'] !== 'hiveextevent_hiveexteventeventrenderslider') {
            return;
        } else {

            $sListTypePlugin = $row['list_type'];

            // ConfigurationManager initilisieren
            $this->configurationManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManager');

            // Das komplette TypoScript holen
            $extbaseFrameworkConfiguration = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);

            // Die gewünschte Konfiguration aus dem Array holen. WICHTIG! die Arrays sind bei TypoScript immer mit einem "."(Punkt) am Ende!
            $config = $extbaseFrameworkConfiguration['plugin.'][$sListTypePlugin.'.']['settings.'];

            // parse xml from flexform to array
            $ffXml = \TYPO3\CMS\Core\Utility\GeneralUtility::xml2array($row['pi_flexform']);


            if ($row['list_type'] !== 'hiveextevent_hiveexteventeventlist') {
                $headerContent .= '<strong>HIVE Events | Slider</strong>';
            } else {
                $headerContent .= '<strong>HIVE Events | List</strong>';
            }


            $sDynamicContentPart = '';

            // show limit in preview
            if($ffXml['data']['oCategories']['lDEF']['settings.categories']['vDEF']){
                $aCategoryTitles = $this->getCategories($ffXml['data']['oCategories']['lDEF']['settings.categories']['vDEF']);


                $sDynamicContentPart .= '
                <tr>
                    <th>Filter for categories</th><td>';

                $i = 0;
                foreach ($aCategoryTitles as $category){
                    $sDynamicContentPart .= '<span style="margin-right: 5px;"><span data-toggle="tooltip" data-placement="top" data-title="" data-original-title="" title=""><span class="t3js-icon icon icon-size-small icon-state-default icon-mimetypes-x-sys_category" data-identifier="mimetypes-x-sys_category"><span class="icon-markup"><img src="/typo3/sysext/core/Resources/Public/Icons/T3Icons/mimetypes/mimetypes-x-sys_category.svg" width="16" height="16"></span></span></span>'.$category['title'].'</span>';
                    $i++;
                }


                $sDynamicContentPart .= '</td></tr>';
            }


            // show limit in preview
            if($ffXml['data']['oList']['lDEF']['settings.iLimit']['vDEF']){
                $sDynamicContentPart .= '
                <tr>
                    <th>Max records displayed</th>
                    <td>'.$ffXml['data']['oList']['lDEF']['settings.iLimit']['vDEF'].'</td>
                </tr>
                ';
            }


            // preview html
            $sContentHtml = '            
            <div class="exampleContent" style="padding: 20px 0 0 0;">
                <table class="table table-condensed table-hover news-table">
                    <thead>
                        <tr>
                            <th colspan="2">
                                <kbd>LIST</kbd>
                            </th>
                        </tr>
                    </thead>
                    <tbody>            
                        <tr>
                            <th>Hide past events automatically</th>
                            <td>'.($config['list.']['bHidePastEventsAutomatically'] ? '<kbd style="background: #AAD400;">Enabled</kbd>' : '<kbd style="background: #C83C3C;">Disabled</kbd>').'</td>
                        </tr>            
                        <tr>
                            <th>Sort direction</th>
                            <td>'.($ffXml['data']['oList']['lDEF']['settings.sOrder']['vDEF'] == 'asc' ? 'Ascending' : 'Descending').'</td>
                        </tr>
                        '.$sDynamicContentPart.'
                    </tbody>
                </table>
            </div>            
            ';

            $itemContent = $sContentHtml;
            $drawItem = false;
        }
    }



    /**
     * Get titles of the selected sys_categories in flexform
     *
     * @param int $sSysCatUids
     * @return string list of categories
     */
    protected function getCategories($sSysCatUids)
    {
        if ($sSysCatUids) {

            // get data
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('sys_category');
            $queryBuilder
                ->select('sys_category.title AS title')
                ->from('sys_category')
                ->where('sys_category.uid IN ('.$sSysCatUids.')')
                ->andWhere('sys_category.hidden = 0')
                ->andWhere('sys_category.deleted = 0')
                ->orderBy('title', 'ASC');

            $oResult = $queryBuilder->execute()->fetchAll();
            return $oResult;

        } else {
            return '';
        }
    }
}