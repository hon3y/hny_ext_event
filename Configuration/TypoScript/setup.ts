
plugin.tx_hiveextevent_hiveexteventeventlist {
    view {
        templateRootPaths.0 = EXT:hive_ext_event/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hiveextevent_hiveexteventeventlist.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_ext_event/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hiveextevent_hiveexteventeventlist.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_ext_event/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hiveextevent_hiveexteventeventlist.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hiveextevent_hiveexteventeventlist.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

plugin.tx_hiveextevent_hiveexteventeventrenderslider {
    view {
        templateRootPaths.0 = EXT:hive_ext_event/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hiveextevent_hiveexteventeventrenderslider.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_ext_event/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hiveextevent_hiveexteventeventrenderslider.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_ext_event/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hiveextevent_hiveexteventeventrenderslider.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hiveextevent_hiveexteventeventrenderslider.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

# these classes are only used in auto-generated templates
plugin.tx_hiveextevent._CSS_DEFAULT_STYLE (
    textarea.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    input.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    .tx-hive-ext-event table {
        border-collapse:separate;
        border-spacing:10px;
    }

    .tx-hive-ext-event table th {
        font-weight:bold;
    }

    .tx-hive-ext-event table td {
        vertical-align:top;
    }

    .typo3-messages .message-error {
        color:red;
    }

    .typo3-messages .message-ok {
        color:green;
    }
)

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
plugin {
    tx_hiveextevent {
        model {
            HIVE\HiveExtEvent\Domain\Model\Event {
                persistence {
                    storagePid = {$plugin.tx_hiveextevent.model.HIVE\HiveExtEvent\Domain\Model\Event.persistence.storagePid}
                }
            }
        }
    }
}

plugin.tx_hiveextevent_hiveexteventeventlist {
    settings {
        list {
            bHidePastEventsAutomatically = {$plugin.tx_hiveextevent_hiveexteventeventlist.settings.list.bHidePastEventsAutomatically}
        }
    }
}

plugin.tx_hiveextevent_hiveexteventeventrenderslider {
    settings {
        slider {
            default {
                background {
                    image = {$plugin.tx_hiveextevent_hiveexteventeventrenderslider.settings.slider.default.background.image}
                }
            }
        }
    }
}