
plugin.tx_hiveextevent_hiveexteventeventlist {
    view {
        # cat=plugin.tx_hiveextevent_hiveexteventeventlist/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_ext_event/Resources/Private/Templates/
        # cat=plugin.tx_hiveextevent_hiveexteventeventlist/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_ext_event/Resources/Private/Partials/
        # cat=plugin.tx_hiveextevent_hiveexteventeventlist/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_ext_event/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hiveextevent_hiveexteventeventlist//a; type=string; label=Default storage PID
        storagePid =
    }
}

plugin.tx_hiveextevent_hiveexteventeventrenderslider {
    view {
        # cat=plugin.tx_hiveextevent_hiveexteventeventrenderslider/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_ext_event/Resources/Private/Templates/
        # cat=plugin.tx_hiveextevent_hiveexteventeventrenderslider/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_ext_event/Resources/Private/Partials/
        # cat=plugin.tx_hiveextevent_hiveexteventeventrenderslider/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_ext_event/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hiveextevent_hiveexteventeventrenderslider//a; type=string; label=Default storage PID
        storagePid =
    }
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
plugin {
    tx_hiveextevent {
        model {
            HIVE\HiveExtEvent\Domain\Model\Event {
                persistence {
                    storagePid =
                }
            }
        }
    }
}

plugin.tx_hiveextevent_hiveexteventeventlist {
    settings {
        list {
            bHidePastEventsAutomatically = 1
        }
    }
}
plugin.tx_hiveextevent_hiveexteventeventrenderslider {
    settings {
        slider {
            default {
                background {
                    image =
                }
            }
        }
    }
}