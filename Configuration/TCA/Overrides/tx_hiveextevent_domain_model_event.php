<?php

defined('TYPO3_MODE') or die();

$sModel = basename(__FILE__, '.php');

$GLOBALS['TCA'][$sModel]['ctrl']['iconfile'] = 'EXT:hive_cpt_brand/Resources/Public/Icons/PNG/hive_16x16.png';

// backend preview items
$GLOBALS['TCA'][$sModel]['ctrl']['label'] = 'title';
$GLOBALS['TCA'][$sModel]['ctrl']['label_alt'] = 'city';
$GLOBALS['TCA'][$sModel]['ctrl']['label_alt_force'] =  1;

// inclusion of external fields (article)
$GLOBALS['TCA'][$sModel]['interface'] = [
    'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, text, logo, zip, city, street, location, country, building, room, booth, start_date, end_date, url, document, intervalvalue, intervalunit, meta_data, taxonomy',
];
$GLOBALS['TCA'][$sModel]['types'] = [
    '1' => ['showitem' => '--div--;General, sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, text, logo, --div--;Timing, start_date, end_date, intervalvalue, intervalunit, --div--;Location, zip, city, street, location, country, building, room, booth, --div--;Further information, url, document, --div--;Meta information, meta_data, taxonomy, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
];
// external fields (article)
$GLOBALS['TCA'][$sModel]['columns']['title'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:hive_ext_article/Resources/Private/Language/locallang_db.xlf:tx_hiveextarticle_domain_model_article.title',
    'config' => [
        'type' => 'input',
        'size' => 30,
        'eval' => 'trim'
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['text'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:hive_ext_article/Resources/Private/Language/locallang_db.xlf:tx_hiveextarticle_domain_model_article.text',
    'config' => [
        'type' => 'text',
        'cols' => 40,
        'rows' => 15,
        'eval' => 'trim',
    ],
    'defaultExtras' => 'richtext:rte_transform'
];
$GLOBALS['TCA'][$sModel]['columns']['meta_data'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:hive_ext_article/Resources/Private/Language/locallang_db.xlf:tx_hiveextarticle_domain_model_article.meta_data',
    'config' => [
        'type' => 'inline',
        'foreign_table' => 'tx_hiveextmetadata_domain_model_metadata',
        'minitems' => 0,
        'maxitems' => 1,
        'appearance' => [
            'collapseAll' => 0,
            'levelLinksPosition' => 'top',
            'showSynchronizationLink' => 1,
            'showPossibleLocalizationRecords' => 1,
            'showAllLocalizationLink' => 1
        ],
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['taxonomy'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:hive_ext_article/Resources/Private/Language/locallang_db.xlf:tx_hiveextarticle_domain_model_article.taxonomy',
    'config' => [
        'type' => 'select',
        'renderType' => '',
        'foreign_table' => 'tx_hiveexttaxonomy_domain_model_tag',
        'MM' => 'tx_hiveextarticle_article_tag_mm',
        'size' => 10,
        'autoSizeMax' => 30,
        'maxitems' => 9999,
        'multiple' => 0,
        'wizards' => [
            '_PADDING' => 1,
            '_VERTICAL' => 1,
            'edit' => [
                'module' => [
                    'name' => 'wizard_edit',
                ],
                'type' => 'popup',
                'title' => 'Edit', // todo define label: LLL:EXT:.../Resources/Private/Language/locallang_tca.xlf:wizard.edit
                'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_edit.gif',
                'popup_onlyOpenIfSelected' => 1,
                'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
            ],
            'add' => [
                'module' => [
                    'name' => 'wizard_add',
                ],
                'type' => 'script',
                'title' => 'Create new', // todo define label: LLL:EXT:.../Resources/Private/Language/locallang_tca.xlf:wizard.add
                'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_add.gif',
                'params' => [
                    'table' => 'tx_hiveexttaxonomy_domain_model_tag',
                    'pid' => '###CURRENT_PID###',
                    'setValue' => 'prepend'
                ],
            ],
        ],
    ],

];


// link wizzard
$GLOBALS['TCA'][$sModel]['columns']['url'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:hive_ext_event/Resources/Private/Language/locallang_db.xlf:tx_hiveextevent_domain_model_event.url',
    'config' => [
        'type' => 'input',
        'size' => '30',
        'eval' => 'trim',
        'wizards' => array(
            'link' => array(
                'type' => 'popup',
                'title' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_link_formlabel',
                'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_link.gif',
                'module' => array(
                    'name' => 'wizard_link',
                ),
                'JSopenParams' => 'height=800,width=600,status=0,menubar=0,scrollbars=1'
            )
        ),
        'softref' => 'typolink'
    ],
];



$GLOBALS['TCA'][$sModel]['columns']['intervalunit'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:hive_ext_event/Resources/Private/Language/locallang_db.xlf:tx_hiveextevent_domain_model_event.intervalunit',
    'config' => [
        'type' => 'select',
        'renderType' => 'selectSingle',
        'items' => [
            ['-- Select Unit --', 0],
            ['Day(s)', 1],
            ['Week(s)', 2],
            ['Month(s)', 3],
            ['Year(s)', 4],
        ],
        'size' => 1,
        'maxitems' => 1,
        'eval' => ''
    ],
];

$GLOBALS['TCA'][$sModel]['columns']['logo'] = [
    'label' => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.images',
    'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
        'logo',
        [
            'appearance' => [
                'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
            ],
            // custom configuration for displaying fields in the overlay/reference table
            // to use the imageoverlayPalette instead of the basicoverlayPalette
            'overrideChildTca' => [
                'types' => [
                    '0' => [
                        'showitem' => '
                                    --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                    --palette--;;filePalette'
                    ],
                    \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                        'showitem' => '
                                    --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                    --palette--;;filePalette'
                    ],
                    \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                        'showitem' => '
                                    --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                    --palette--;;filePalette'
                    ],
                    \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                        'showitem' => '
                                    --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.audioOverlayPalette;audioOverlayPalette,
                                    --palette--;;filePalette'
                    ],
                    \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                        'showitem' => '
                                    --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.videoOverlayPalette;videoOverlayPalette,
                                    --palette--;;filePalette'
                    ],
                    \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                        'showitem' => '
                                    --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                    --palette--;;filePalette'
                    ]
                ],
            ],
        ],
        $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
    )
];


// date field (start)
$GLOBALS['TCA'][$sModel]['columns']['start_date']['config']['size'] = '30';
$GLOBALS['TCA'][$sModel]['columns']['start_date']['config']['default'] = 0;
// date field (end)
$GLOBALS['TCA'][$sModel]['columns']['end_date']['config']['size'] = '30';
$GLOBALS['TCA'][$sModel]['columns']['end_date']['config']['default'] = 0;


// requirements
$GLOBALS['TCA'][$sModel]['columns']['title']['config']['eval'] = 'trim,required';
$GLOBALS['TCA'][$sModel]['columns']['start_date']['config']['eval'] = 'datetime,required';
