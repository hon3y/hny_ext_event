<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveExtEvent',
            'Hiveexteventeventlist',
            [
                'Event' => 'list, show'
            ],
            // non-cacheable actions
            [
                'Event' => 'list, show'
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveExtEvent',
            'Hiveexteventeventrenderslider',
            [
                'Event' => 'renderSlider'
            ],
            // non-cacheable actions
            [
                'Event' => 'renderSlider'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    hiveexteventeventlist {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_ext_event') . 'Resources/Public/Icons/user_plugin_hiveexteventeventlist.svg
                        title = LLL:EXT:hive_ext_event/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_event_domain_model_hiveexteventeventlist
                        description = LLL:EXT:hive_ext_event/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_event_domain_model_hiveexteventeventlist.description
                        tt_content_defValues {
                            CType = list
                            list_type = hiveextevent_hiveexteventeventlist
                        }
                    }
                    hiveexteventeventrenderslider {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_ext_event') . 'Resources/Public/Icons/user_plugin_hiveexteventeventrenderslider.svg
                        title = LLL:EXT:hive_ext_event/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_event_domain_model_hiveexteventeventrenderslider
                        description = LLL:EXT:hive_ext_event/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_event_domain_model_hiveexteventeventrenderslider.description
                        tt_content_defValues {
                            CType = list
                            list_type = hiveextevent_hiveexteventeventrenderslider
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
call_user_func(
    function($extKey, $globals)
    {

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins.elements.hiveexteventeventlist >
                wizards.newContentElement.wizardItems {
                    hive {
                        header = Hive
                        after = common,special,menu,plugins,forms
                        elements.hiveexteventeventlist {
                            iconIdentifier = hive_cpt_brand_32x32_svg
                            title = Event (list)
                            description = List of events eg. fairs
                            tt_content_defValues {
                                CType = list
                                list_type = hiveextevent_hiveexteventeventlist
                            }
                        }
                        show := addToList(hiveexteventeventlist)
                    }

                }
            }'
        );
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins.elements.hiveexteventeventrenderslider >
                wizards.newContentElement.wizardItems {
                    hive {
                        header = Hive
                        after = common,special,menu,plugins,forms
                        elements.hiveexteventeventrenderslider {
                            iconIdentifier = hive_cpt_brand_32x32_svg
                            title = Event (slider)
                            description = Slider of events eg. fairs
                            tt_content_defValues {
                                CType = list
                                list_type = hiveextevent_hiveexteventeventrenderslider
                            }
                        }
                        show := addToList(hiveexteventeventrenderslider)
                    }

                }
            }'
        );

        // Register for hook to show preview of tt_content element of CType="yourextensionkey_newcontentelement" in page module
        $globals['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem'][$extKey] =
            \HIVE\HiveExtEvent\Hooks\PageLayoutView\EventListPreviewRenderer::class;

    }, $_EXTKEY, $GLOBALS
);