<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveExtEvent',
            'Hiveexteventeventlist',
            'hive_ext_event :: Event :: list'
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveExtEvent',
            'Hiveexteventeventrenderslider',
            'hive_ext_event :: Event :: renderSlider'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_ext_event', 'Configuration/TypoScript', 'hive_ext_event');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextevent_domain_model_event', 'EXT:hive_ext_event/Resources/Private/Language/locallang_csh_tx_hiveextevent_domain_model_event.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextevent_domain_model_event');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
            'hive_ext_event',
            'tx_hiveextevent_domain_model_event'
        );

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
// Name of the extension
$sExtensionName = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY));
// Array with plugin names
$aPluginNames = [
    0 => strtolower('Hiveexteventeventlist'),
    1 => strtolower('Hiveexteventeventrenderslider')
];
// Init flexform files for each plugin
foreach ($aPluginNames as $aPluginName) {
    $sPluginSignature = $sExtensionName.'_'.$aPluginName;
    $TCA['tt_content']['types']['list']['subtypes_excludelist'][$sPluginSignature] = 'layout,select_key,pages,recursive';
    $TCA['tt_content']['types']['list']['subtypes_addlist'][$sPluginSignature] = 'pi_flexform';
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($sPluginSignature, 'FILE:EXT:'.$_EXTKEY . '/Configuration/FlexForms/'. ucfirst($aPluginName) .'.xml');
}